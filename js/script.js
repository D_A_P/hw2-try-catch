const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

  const root = document.getElementById('root');
  const ul = document.createElement('ul');

  books.forEach((book) => {
    try{
      if (!book.author){
        throw new Error('author is not in the book')
      }
      if (!book.name){
        throw new Error('name is not in the book')
      }
      if (!book.price){
        throw new Error('price is not in the book')
      }
      const li = document.createElement('li');
      li.textContent = `${book.name}, ${book.author}, ${book.price}`;
      ul.append(li);
    } catch(error) {
      console.log(error.message);
    }
  });

  root.append(ul);